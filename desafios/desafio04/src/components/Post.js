import React from "react";
import { render } from "react-dom";
import user from "../assets/user.png";
import Comments from "./Comments";

function Post({ data }) {
  return (
    <div className="post">
      <div className="photo">
        <img src={user} />
        <div className="photo-name">
          <span className="photo-user">{data.author.name}</span>
          <span className="photo-date">{data.date}</span>
        </div>
      </div>
      <div className="content">{data.content}</div>
      <hr className="divider"></hr>
      {data.comments.map(comment => (
        <Comments key={comment.id} img={user} data={comment} />
      ))}
    </div>
  );
}

export default Post;
