import React, { Component } from "react";
import facebook from "../assets/facebook.png";
import { FaUserCircle } from "react-icons/fa";

class Header extends Component {
  render() {
    return (
      <header>
        <div className="header space-between">
          <div>
            <img width="120" src={facebook} />
          </div>
          <div>
            <span className="perfil e">
              Meu Perfil <FaUserCircle />
            </span>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;
