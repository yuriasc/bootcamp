import React from "react";

function Comments({ img, data }) {
  return (
    <div className="comment">
      <div className="img">
        <img src={img} />
      </div>
      <div className="text">
        <span className="author">{data.author.name} </span>
        {data.content}
      </div>
    </div>
  );
}

export default Comments;
